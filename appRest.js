var dato = document.getElementById('personaje');
dato.addEventListener('change', function () {
    var personaje = document.getElementById('personaje').value;
    getAllRequest(personaje);
});
function getAllRequest(data) {
    axios.get("https://rickandmortyapi.com/api/character?name=" + data)
        .then(function (response) {
        var resultados = response.data.results;
        var card = document.getElementById('card');
        for (var index = 0; index < resultados.length; index++) {
            var elemento = resultados[index];
            console.log(elemento);
            var h1 = document.createElement('h1');
            var p = document.createElement('p');
            var imagen = document.createElement('img');
            h1.textContent = elemento.name;
            p.textContent = elemento.status;
            imagen.setAttribute('src', elemento.image);
            card.appendChild(h1);
            card.appendChild(p);
            card.appendChild(imagen);
        }
    });
}

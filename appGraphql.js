const dato = document.getElementById('personaje')
dato.addEventListener('change',function(){
    const personaje = document.getElementById('personaje').value
    console.log(personaje)

    let query = {
        query: `
          query obtener($name: String){
            characters(filter: { name: $name }) {
                results {
                  image
                  name
                  status
                }
              }
          }`,
          variables: `{
              "name": "${personaje}"
          }`
      }


      let xhr = new XMLHttpRequest()
      xhr.responseType = 'json'
      xhr.open('POST', 'https://rickandmortyapi.com/graphql')
      xhr.setRequestHeader('Content-Type', 'application/json')
      
      xhr.onload = function () {
        const resultados = xhr.response.data.characters.results
        const card = document.getElementById('card')
        for (let index = 0; index < resultados.length; index++) {
            const element = resultados[index];

            var h1 = document.createElement('h1');
            var p = document.createElement('p');
            var imagen = document.createElement('img');
            h1.textContent = element.name;
            p.textContent = element.status;
            imagen.setAttribute('src', element.image);
            card.appendChild(imagen);
            card.appendChild(h1);
            card.appendChild(p);
            
        }
      }
      
      xhr.send(JSON.stringify(query))
})  
